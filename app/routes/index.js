import postRoutes from './postRoutes';
import commentRoutes from './commentRoutes';

export default (app) => {
    postRoutes(app);
    commentRoutes(app);
}