import Vue from 'vue';
import App from './components/App.vue'
Vue.config.devtools = true;
new Vue({
    render: h => h(App),
}).$mount('#app');